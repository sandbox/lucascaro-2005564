<?php
/**
 * @file
 * PASH Administration functions.
 */


function pash_admin_form() {
  $form['core'] = array(
    '#type' => 'fieldset',
    '#title' => t('Core Settings'),
    '#description' => t('These are performance improvements for drupal core.'),
    '#collapsible' => TRUE,
  );
  $form['core'] = array_merge($form['core'], pash_settings_core());
  if (module_exists('views')) {
    $form['views'] = array(
      '#type' => 'fieldset',
      '#title' => t('Views Settings'),
      '#description' => t('These are performance improvements for views. Checking a box will add a time based cache to that display (5 minutes). Note that caching the default display will result in caching all displays for most cases (unless that particular view has overriden settings).'),
      '#tree' => TRUE,
      '#collapsible' => TRUE,
    );
    $form['views'] = array_merge($form['views'], pash_settings_views());
  }
  if (module_exists('panels')) {
    $form['panels'] = array(
      '#type' => 'fieldset',
      '#title' => t('Panels Settings'),
      '#description' => t('These are performance improvements for panels. Checking a display will set up a 15 second simple cache for it.'),
      '#tree' => TRUE,
      '#collapsible' => TRUE,
    );
    $form['panels'] = array_merge($form['panels'], pash_settings_panels());
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}


function pash_settings_core() {
  $performance_link = l(t('show advanced settings'), 'admin/config/development/performance');
  $form['cache'] = array(
    '#title' => t('Enable page cache'),
    '#description' => t('Check to enable page cache or !advanced', array(
      '!advanced' => $performance_link
    )),
    '#type' => 'checkbox',
    '#default_value' => variable_get('cache', FALSE),
  );
  $form['page_compression'] = array(
    '#title' => t('Compress cached pages.'),
    '#description' => t('Check to enable page compression or !advanced', array(
      '!advanced' => $performance_link,
    )),
    '#type' => 'checkbox',
    '#default_value' => variable_get('page_compression', TRUE),
  );
  $form['preprocess_css'] = array(
    '#title' => t('Aggregate and compress CSS files.'),
    '#description' => t('Check to enable css compression or !advanced', array(
      '!advanced' => $performance_link,
    )),
    '#type' => 'checkbox',
    '#default_value' => variable_get('preprocess_css', TRUE),
  );
  $form['preprocess_js'] = array(
    '#title' => t('Aggregate JavaScript files.'),
    '#description' => t('Check to enable js compression or !advanced', array(
      '!advanced' => $performance_link,
    )),
    '#type' => 'checkbox',
    '#default_value' => variable_get('preprocess_js', TRUE),
  );

  return $form;
}

function pash_settings_views() {
  $views = views_get_all_views();
  $form = array();
  foreach ($views as $v => $view) {
    $view->init_display();
    foreach ($view->display as $d => $display) {
      $cache = $display->handler->get_plugin('cache');
      if (empty($cache) || $cache instanceof views_plugin_cache_none) {
        $targs = array(
          '!view' => $view->name,
          '!display' => $d,
        );
        $form[$view->name][$d]['cache'] = array(
          '#title' => t('Cache !view:!display', $targs),
          '#description' => t('Enable caching for the view !view on display !display.', $targs),
          '#type' => 'checkbox',
          '#default_value' => FALSE,
        );
      }
    }
  }
  if (empty($form)) {
    $form['empty'] = array(
      '#type' => 'item',
      '#title' => t('No uncached views detected.'),
    );
  }
  return $form;
}

function pash_settings_panels() {
  $form = array();
  $tasks = page_manager_get_tasks_by_type('page');
  $pages = array('operations' => array(), 'tasks' => array());
  module_load_include('admin.inc','page_manager');
  page_manager_get_pages($tasks, $pages);
  foreach ($pages['rows'] as $page_name => $page) {
    $page = page_manager_cache_load($page_name);
    foreach ($page->handlers as $h => $handler) {
      $display = NULL;
      if (isset($handler->conf['display'])) {
        $display = $handler->conf['display'];
      }
      else if (isset($handler->conf['did'])) {
        $display = panels_load_display($handler->conf['did']);
      }
      if (!empty($display) && empty($display->cache)) {
        $form[$page_name][$h][$display->did] = array(
          '#type' => 'checkbox',
          '#title' => t('Cache !page : !handler', array('!page' => $page_name, '!handler' => $h)),
          '#description' => t('Check this box to enable caching for this page or !link', array(
            '!link' => l('configure manually', "admin/structure/pages/nojs/operation/$page_name/handlers/$h/content"),
          )),
          '#default_value' => FALSE,
        );
      }
    }
  }
  return $form;
}

function pash_admin_form_submit($form, $form_state) {
  $values = $form_state['values'];
  // Core tweaks.
  variable_set('cache', $values['cache']);
  variable_set('page_compression', $values['page_compression']);
  variable_set('preprocess_css', $values['preprocess_css']);
  variable_set('preprocess_js', $values['preprocess_js']);

  // Views Caching.
  foreach ($values['views'] as $v => $view) {
    foreach ($view as $d => $display) {
      if ($display['cache']) {
        // Enable cache for this display.
        $view = views_get_view($v);
        $view->set_display();
        $view->display_handler->override_option('cache', array(
          'type' => 'time',
          'results_lifespan' => '300',
          'output_lifespan' => '300',
        ));
        views_save_view($view);
      }
    }
  }
  // Panels Caching.
  foreach ($values['panels'] as $page_name => $panel) {
    foreach ($panel as $handler_name => $handler) {
      foreach ($handler as $did => $enable_cache) {
        if ($enable_cache) {
          if (is_numeric($did)) {
            $display = panels_load_display($did);
          }
          else {
            // Overriding a display defined in code.
            $page = page_manager_cache_load($page_name);
            $handler = $page->handlers[$handler_name];
            if (isset($handler->conf['display'])) {
              $display = $handler->conf['display'];
            }
            else if (isset($handler->conf['did'])) {
              $display = panels_load_display($handler->conf['did']);
            }
          }
          $display->cache = array(
            'method' => 'simple',
            'settings' => array(
              'lifetime' => 15,
              'granularity' => 'args',
            ),
          );
          $handler->conf['display'] = $display;
          page_manager_save_task_handler($handler);
        }
      }
    }
  }
}
